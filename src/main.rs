use plotters::prelude::*;

struct Vector {
    id: u64,
    data: Vec<f64>,
}

struct VectorDB {
    vectors: Vec<Vector>,
}

impl VectorDB {
    fn new() -> Self {
        Self { vectors: Vec::new() }
    }

    fn add_vector(&mut self, vector: Vector) {
        self.vectors.push(vector);
    }

    fn nearest_neighbor(&self, query: &Vector) -> Option<&Vector> {
        self.vectors.iter().min_by(|a, b| {
            let dist_a = euclidean_distance(&a.data, &query.data);
            let dist_b = euclidean_distance(&b.data, &query.data);

            dist_a.partial_cmp(&dist_b).unwrap()
        })
    }

    fn average_vector(&self) -> Option<Vector> {
        if self.vectors.is_empty() {
            return None;
        }

        let sum_vector: Vec<f64> = self.vectors.iter().fold(vec![0f64; self.vectors[0].data.len()], |mut acc, vec| {
            for (i, &val) in vec.data.iter().enumerate() {
                acc[i] += val;
            }
            acc
        });

        let avg_vector: Vec<f64> = sum_vector.into_iter().map(|val| val / self.vectors.len() as f64).collect();

        Some(Vector { id: 0, data: avg_vector })
    }
}

fn euclidean_distance(a: &[f64], b: &[f64]) -> f64 {
    a.iter().zip(b.iter()).map(|(x, y)| (x - y).powi(2)).sum::<f64>().sqrt()
}

fn visualize_vector(vector: &Vector, file_path: &str) -> Result<(), Box<dyn std::error::Error>> {
    let root = BitMapBackend::new(file_path, (640, 480)).into_drawing_area();
    root.fill(&WHITE)?;

    let max_value = vector.data.iter().cloned().fold(f64::MIN, f64::max);
    let min_value = vector.data.iter().cloned().fold(f64::MAX, f64::min);

    let mut chart = ChartBuilder::on(&root)
        .caption("Average Vector Visualization", ("sans-serif", 40))
        .margin(5)
        .x_label_area_size(30)
        .y_label_area_size(30)
        .build_cartesian_2d(0..vector.data.len(), min_value..max_value)?;

    chart.configure_mesh().draw()?;

    chart.draw_series(LineSeries::new(
        vector.data.iter().enumerate().map(|(i, &val)| (i, val)),
        &RED,
    ))?;

    Ok(())
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let mut db = VectorDB::new();

    // Data Ingestion
    db.add_vector(Vector { id: 1, data: vec![1.0, 2.0, 3.0] });
    db.add_vector(Vector { id: 2, data: vec![4.0, 5.0, 6.0] });
    db.add_vector(Vector { id: 3, data: vec![7.0, 8.0, 9.0] });

    // Nearest Neighbor Query
    let query_vector = Vector { id: 4, data: vec![5.0, 5.0, 5.0] };
    if let Some(nearest) = db.nearest_neighbor(&query_vector) {
        println!("The nearest neighbor is Vector with id: {}", nearest.id);
    }

    // Aggregating Average Vector
    if let Some(avg_vector) = db.average_vector() {
        println!("Average Vector: {:?}", avg_vector.data);

        // Visualization
        visualize_vector(&avg_vector, "average_vector.png")?;
    }

    Ok(())
}
